package ru.tsc.almukhametov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TerminalConst {

    @NotNull
    public static final String ABOUT = "about";
    @NotNull
    public static final String HELP = "help";
    @NotNull
    public static final String VERSION = "version";
    @NotNull
    public static final String INFO = "info";
    @NotNull
    public static final String COMMANDS = "commands";
    @NotNull
    public static final String ARGUMENTS = "arg";
    @NotNull
    public static final String PROJECT_LIST = "project-list";
    @NotNull
    public static final String PROJECT_CREATE = "project-create";
    @NotNull
    public static final String PROJECT_CLEAR = "project-clear";
    @NotNull
    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";
    @NotNull
    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";
    @NotNull
    public static final String PROJECT_SHOW_BY_NAME = "project-show-by-name";
    @NotNull
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    @NotNull
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    @NotNull
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    @NotNull
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";
    @NotNull
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    @NotNull
    public static final String PROJECT_START_BY_ID = "project-start-by-id";
    @NotNull
    public static final String PROJECT_START_BY_INDEX = "project-start-by-index";
    @NotNull
    public static final String PROJECT_START_BY_NAME = "project-start-by-name";
    @NotNull
    public static final String PROJECT_FINISH_BY_ID = "project-finish-by-id";
    @NotNull
    public static final String PROJECT_FINISH_BY_INDEX = "project-finish-by-index";
    @NotNull
    public static final String PROJECT_FINISH_BY_NAME = "project-finish-by-name";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";
    @NotNull
    public static final String PROJECT_CHANGE_STATUS_BY_NAME = "project-change-status-by-name";
    @NotNull
    public static final String TASK_LIST = "task-list";
    @NotNull
    public static final String TASK_CREATE = "task-create";
    @NotNull
    public static final String TASK_CLEAR = "task-clear";
    @NotNull
    public static final String TASK_SHOW_BY_ID = "task-show-by-id";
    @NotNull
    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";
    @NotNull
    public static final String TASK_SHOW_BY_NAME = "task-by-name";
    @NotNull
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    @NotNull
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    @NotNull
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    @NotNull
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    @NotNull
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    @NotNull
    public static final String TASK_START_BY_ID = "task-start-by-id";
    @NotNull
    public static final String TASK_START_BY_INDEX = "task-start-by-index";
    @NotNull
    public static final String TASK_START_BY_NAME = "task-start-by-name";
    @NotNull
    public static final String TASK_FINISH_BY_ID = "task-finish-by-id";
    @NotNull
    public static final String TASK_FINISH_BY_INDEX = "task-finish-by-index";
    @NotNull
    public static final String TASK_FINISH_BY_NAME = "task-finish-by-name";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";
    @NotNull
    public static final String TASK_CHANGE_STATUS_BY_NAME = "task-change-status-by-name";
    @NotNull
    public static final String TASK_LIST_BY_PROJECT = "task-list-by-project";
    @NotNull
    public static final String TASK_BIND_TO_PROJECT = "task-bind-to-project";
    @NotNull
    public static final String TASK_UNBIND_FROM_PROJECT = "task-bind-from-project";
    @NotNull
    public static final String USER_LIST = "user-list";
    @NotNull
    public static final String USER_CREATE = "user-registry";
    @NotNull
    public static final String USER_CLEAR = "user-clear";
    @NotNull
    public static final String USER_SHOW_BY_ID = "user-show-by-id";
    @NotNull
    public static final String USER_SHOW_BY_LOGIN = "user-show-by-login";
    @NotNull
    public static final String USER_REMOVE_BY_ID = "user-remove-by-id";
    @NotNull
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    @NotNull
    public static final String USER_UPDATE_BY_ID = "user-update-by-id";
    @NotNull
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";
    @NotNull
    public static final String USER_CHANGE_PASSWORD = "user-change-password";
    @NotNull
    public static final String USER_CHANGE_ROLE = "user-change-role";
    @NotNull
    public static final String USER_LOCK_BY_LOGIN = "user-lock-by-login";
    @NotNull
    public static final String USER_UNLOCK_BY_LOGIN = "user-unlock-by-login";
    @NotNull
    public static final String DATA_BIN_LOAD = "data-bin-load";
    @NotNull
    public static final String DATA_BIN_SAVE = "data-bin-save";
    @NotNull
    public static final String DATA_BASE64_LOAD = "data-base64-load";
    @NotNull
    public static final String DATA_BASE64_SAVE = "data-base64-save";
    @NotNull
    public static final String LOGIN = "login";
    @NotNull
    public static final String LOGOUT = "logout";
    @NotNull
    public static final String EXIT = "exit";

}
