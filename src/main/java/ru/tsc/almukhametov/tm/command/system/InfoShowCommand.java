package ru.tsc.almukhametov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.util.UnitUtil;

public final class InfoShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.INFO;
    }

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.INFO;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.INFO;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + UnitUtil.convertBytes(freeMemory));
        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = UnitUtil.convertBytes(maxMemory);
        @NotNull final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + UnitUtil.convertBytes(totalMemory));
        @NotNull final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + UnitUtil.convertBytes(usedMemory));
    }

}
