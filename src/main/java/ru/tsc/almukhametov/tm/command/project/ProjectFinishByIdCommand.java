package ru.tsc.almukhametov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_FINISH_BY_ID;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_FINISH_BY_ID;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("Enter Id");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().finishById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        else project.setFinishDate(new Date());
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
