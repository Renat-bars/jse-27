package ru.tsc.almukhametov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractProjectCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_SHOW_BY_NAME;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_SHOW_BY_NAME;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("Enter Name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Optional<Project> project = serviceLocator.getProjectService().findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        showProject(project.get());
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
