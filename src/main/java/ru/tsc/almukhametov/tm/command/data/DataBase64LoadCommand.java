package ru.tsc.almukhametov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.dto.Domain;
import ru.tsc.almukhametov.tm.enumerated.Role;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.DATA_BASE64_LOAD;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return SystemDescriptionConst.DATA_BASE64_LOAD;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        @NotNull final byte[] decodedData = Base64.getDecoder().decode(base64);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        byteArrayInputStream.close();
        objectInputStream.close();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
