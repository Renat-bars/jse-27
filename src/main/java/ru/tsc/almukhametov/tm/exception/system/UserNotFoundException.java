package ru.tsc.almukhametov.tm.exception.system;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User was not found");
    }
}
