package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException(@NotNull final String login) {
        super("Incorrect argument. Argument ``" + login + "`` was not founded. " +
                "Use ``" + ArgumentConst.HELP + "`` for display list of arguments");
    }
}
