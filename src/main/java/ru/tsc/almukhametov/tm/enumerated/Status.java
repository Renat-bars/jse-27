package ru.tsc.almukhametov.tm.enumerated;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

public enum Status {

    @NotNull
    NOT_STARTED("Not started"),
    @NotNull
    IN_PROGRESS("In progress"),
    @NotNull
    COMPLETED("Complete");

    @Getter
    @Setter
    private String displayName;

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

}
