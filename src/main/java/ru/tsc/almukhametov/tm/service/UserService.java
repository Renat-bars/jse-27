package ru.tsc.almukhametov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IUserRepository;
import ru.tsc.almukhametov.tm.api.service.IPropertyService;
import ru.tsc.almukhametov.tm.api.service.IUserService;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.*;
import ru.tsc.almukhametov.tm.exception.system.EmailExistsException;
import ru.tsc.almukhametov.tm.exception.system.LoginExistsException;
import ru.tsc.almukhametov.tm.exception.system.UserNotFoundException;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull IUserRepository userRepository, @NotNull IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        setPassword(user, password);
        user.setRole(Role.USER);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException(login);
        if (isEmailExists(email)) throw new EmailExistsException(email);
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    public Optional<User> findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        return userRepository.findByLogin(login).isPresent();
    }

    @NotNull
    @Override
    public Optional<User> findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @NotNull
    @Override
    public Optional<User> removeUser(@Nullable final User user) {
        if (user == null) return Optional.empty();
        return userRepository.removeUser(user);
    }

    @NotNull
    @Override
    public Optional<User> removeUserById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeUserById(id);
    }

    @NotNull
    @Override
    public Optional<User> removeUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeUserByLogin(login);
    }

    @Override
    public User updateUser(final String userId, final @Nullable String firstName, @Nullable final String lastName,
                           @Nullable final String middleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        return user.get();
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new EmptyIdException();
        setPassword(String.valueOf(user), password);
        return user.get();
    }

    @NotNull
    @Override
    public User setRole(@Nullable final String userId, @Nullable final Role role) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyPasswordException();
        @Nullable final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new EmptyIdException();
        user.get().setRole(role);
        return user.get();
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final Optional<User> user = userRepository.findByLogin(login);
        if (!user.isPresent()) throw new EmptyIdException();
        user.get().setLocked(true);
        return user.get();
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final Optional<User> user = userRepository.findByLogin(login);
        if (!user.isPresent()) throw new EmptyIdException();
        user.get().setLocked(false);
        return user.get();
    }

    @NotNull
    private void setPassword(@NotNull final User user, @NotNull final String password) {
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final String passwordHash = HashUtil.salt(password, secret, iteration);
        user.setPasswordHash(passwordHash);
    }

}