package ru.tsc.almukhametov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.api.service.IProjectService;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.empty.*;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.Optional;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @NotNull
    @Override
    public Optional<Project> findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Optional<Project> removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name,
                              @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Optional<Project> project = projectRepository.findById(userId, id);
        project.ifPresent(p -> p.setName(name));
        project.ifPresent(p -> p.setDescription(description));
        return project.get();
    }

    @NotNull
    @Override
    public Project updateByIndex(@Nullable final String userId, final Integer index, final @Nullable String name,
                                 final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Optional<Project> project = projectRepository.findByIndex(userId, index);
        project.ifPresent(p -> p.setName(name));
        project.ifPresent(p -> p.setDescription(description));
        return project.get();
    }

    @NotNull
    @Override
    public Project startById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @NotNull
    @Override
    public Project startByIndex(@Nullable final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.startByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project startByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @NotNull
    @Override
    public Project finishById(final @Nullable String userId, final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.finishById(userId, id);
    }

    @NotNull
    @Override
    public Project finishByIndex(final @Nullable String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeProjectStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeProjectStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public Project changeProjectStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeProjectStatusByName(userId, name, status);
    }

}

